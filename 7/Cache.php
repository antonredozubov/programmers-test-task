<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Cache interface
 *
 * @category  ProgrammerTask
 * @package   7
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

interface Cache
{
    function cget($id);

    function cset($id, $value);

    function cdelete($id);
} 