<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Abstract model class
 *
 * @category  ProgrammerTask
 * @package   7
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

abstract class Model
{
    protected $_pdo = false;
    private $_new = false;

    public function __construct($scenario = true)
    {

        $this->_pdo = PDO();

        if ($scenario === null) // internally used by model()
        {
            return;
        }

        $this->setIsNewRecord(true);
    }

    public function setIsNewRecord($value)
    {
        $this->_new = $value;
    }

    /**
     * Pseudo Singltone
     *
     * @param string $className
     *
     * @return mixed
     */
    public static function model($className = __CLASS__)
    {
        if (isset(self::$_models[$className]))
        {
            return self::$_models[$className];
        }
        else
        {
            $model = self::$_models[$className] = new $className(null);
            return $model;
        }
    }

    public function save($attributes = null)
    {
        return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
    }

    public function getIsNewRecord()
    {
        return $this->_new;
    }

    abstract public function insert($attributes = null);

    abstract public function update($attributes = null);

    abstract public function drop($pk);

    abstract public function findByPk($pk);

    abstract public function findAll();


} 