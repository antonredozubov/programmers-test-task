<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Cached news model
 *
 * @category  ProgrammerTask
 * @package   7
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class CachedNews extends News implements Cache
{
    private $_mc = false;

    public function __construct()
    {
        $this->_mc = new Memcache;
        $this->_mc->connect('localhost', 11211) or die (';(');

        parent::__construct();
    }

    public function update($attributes = null)
    {
        if (!empty($attributes) and is_array($attributes) and array_key_exists('id', $attributes))
        {
            $this->cdelete($attributes['id']);

            return parent::update($attributes);
        }
        else
        {
            throw new Exception('Wrong attributes');
        }
    }

    public function drop($pk)
    {
        $this->cdelete($pk);
        
        return $this->_pdo->prepare('delete from news where id=:id')->bindParam(':id', $pk)->execute();
    }

    public function findAll()
    {
        $all_news = [];
        $ids      = $this->_pdo->prepare('select id from news')->execute()->fetchAll(PDO::FETCH_COLUMN, 0);

        foreach ($ids as $id)
        {
            $all_news[] = $this->findByPk($id);
        }

        return $all_news;
    }

    public function findByPk($pk)
    {
        if (!$data = json_decode($this->cget($pk)))
        {
            $data = parent::findByPk($pk);
            $this->cset($pk, json_encode($data));
        }

        return $data;
    }

    function cget($id)
    {
        return $this->_mc->get($id);
    }

    function cset($id, $value)
    {
        return $this->_mc->set($id, $value) or die (':(');
    }

    function cdelete($id)
    {
        return $this->_mc->delete($id);
    }
} 