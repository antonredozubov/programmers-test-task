<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * News model
 *
 * @category  ProgrammerTask
 * @package   7
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class News extends Model
{
    protected $data = [];

    public function insert($attributes = null)
    {
        if (!empty($attributes) and is_array($attributes) and array_key_exists('title', $attributes) and array_key_exists('text', $attributes))
        {
            $this->_pdo->prepare('insert into news (title, text) values (:title, :text)')->bindParam(':title', $attributes['title'])->bindParam(':text', $attributes['text'])->execute();

            return $this->_pdo->lastInsertId();
        }
        else
        {
            throw new Exception('Wrong attributes');
        }
    }

    public function update($attributes = null)
    {
        if (!empty($attributes) and is_array($attributes) and array_key_exists('id', $attributes) and array_key_exists('title', $attributes) and array_key_exists('text', $attributes))
        {
            $this->_pdo->prepare('update news set title=:title, text=:text where id=:id')->bindParam(':title', $attributes['title'])->bindParam(':text', $attributes['text'])->bindParam(':id', $attributes['id'])->execute();

            return $this->_pdo->lastInsertId();
        }
        else
        {
            throw new Exception('Wrong attributes');
        }
    }

    public function drop($pk)
    {
        return $this->_pdo->prepare('delete from news where id=:id')->bindParam(':id', $pk)->execute();
    }

    public function findByPk($pk)
    {
        $this->data = $this->_pdo->prepare('select title, text, createts from news where id=:id')->bindParam(':id', $pk)->execute()->fetch(PDO::FETCH_BOTH);

        return $this->data;
    }

    public function findAll()
    {
        return $this->_pdo->prepare('select title, text, createts from news')->execute()->fetchAll();
    }
} 