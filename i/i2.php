<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * XSLT
 *
 * PHP 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ProgrammerTask
 * @package   I
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

/**
 * Class and Function List:
 * Function list:
 * - echoHelpAndDie()
 */

/**
 * Echo help message and terminate
 */
function echoHelpAndDie()
{
    echo 'Usage: 2.php [-h] -m <xml> -s <xsl> [-o <outputfile>]'.PHP_EOL.
        PHP_EOL.
        '  -h, --help    This help'.PHP_EOL.
        '  -m, --xml     XML file'.PHP_EOL.
        '  -s, --xsl     XSL file'.PHP_EOL.
        '  -o, --output  File to save result HTML'.PHP_EOL.
        PHP_EOL;
    die();
}

if (PHP_SAPI === 'cli' or empty($_SERVER['REMOTE_ADDR']))
{
    // init from args
    $shortopts = 'hm:s:o:';

    $longopts = array(
        'help',
        'xml:',
        'xsl:',
        'output:',
    );

    $options = getopt($shortopts, $longopts);

    if (array_key_exists('help', $options) or array_key_exists('h', $options))
    {
        echoHelpAndDie();
    }
    else if (!(
        (array_key_exists('m', $options) or array_key_exists('xml', $options))
        and (array_key_exists('s', $options) or array_key_exists('xsl', $options)))
    )
    {
        echoHelpAndDie();
    }

    $xmlFile = (array_key_exists('m', $options) ? $options['m'] : $options['xml']);
    $xslFile = (array_key_exists('s', $options) ? $options['s'] : $options['xsl']);

    if (!(is_readable($xmlFile) and is_readable($xslFile)))
    {
        echo 'Error: input files is not readable'.PHP_EOL;
        echoHelpAndDie();
    }

    $xsl = new XSLTProcessor();

    $xsldoc = new DOMDocument();
    $xsldoc->load($xslFile);
    $xsl->importStyleSheet($xsldoc);

    $xmldoc = new DOMDocument();
    $xmldoc->load($xmlFile);

    $html = $xsl->transformToXML($xmldoc);

    if (array_key_exists('output', $options) or array_key_exists('o', $options))
    {
        $outputFile = (array_key_exists('o', $options) ? $options['o'] : $options['output']);
        if (false === file_put_contents($outputFile, $html))
        {
            throw new Exception('File write error');
        }
        echo 'HTML successfully writed to file'.PHP_EOL;
    }
    else
    {
        echo $html;
    }
}
else if (isset($_REQUEST))
{
    throw new Exception('CLI only');
}