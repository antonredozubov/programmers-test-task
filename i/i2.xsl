<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="inRow" select="3" />

    <xsl:output method="html" indent="yes" encoding="UTF-8" />

    <xsl:template match="treeNode[@name = 'root']">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <style type="text/css">div.treeNode { padding-left: 20px; } table td { border: 1px solid black; }</style>
            </head>
            <body>
                <div class="treeNode">
                    <xsl:value-of select="@name" />
                    <xsl:apply-templates select="treeNode" />
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="treeNode[@name != 'root' and not(treeNode)]">
        <xsl:if test="position() = 1">
            <xsl:text disable-output-escaping="yes">&lt;div class="treeNode"&gt;&lt;table cellpadding="1" cellspacing="1"&gt;&lt;tr&gt;</xsl:text>
        </xsl:if>
        <xsl:if test="position() > 1 and position() mod $inRow = 1">
            <xsl:text disable-output-escaping="yes">&lt;/tr&gt;&lt;tr&gt;</xsl:text>
        </xsl:if>

        <td><xsl:value-of select="@name" /></td>

        <xsl:if test="position() = last()">
            <xsl:text disable-output-escaping="yes">&lt;/tr&gt;&lt;/table&gt;&lt;/div&gt;</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="treeNode[@name != 'root' and treeNode]">
        <div class="treeNode">
            <xsl:value-of select="@name" />
            <xsl:apply-templates select="treeNode" />
        </div>
    </xsl:template>

</xsl:stylesheet>