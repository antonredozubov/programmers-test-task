<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Generate DOM-tree
 *
 * PHP 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ProgrammerTask
 * @package   I
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

/**
 * Class and Function List:
 * Function list:
 * - fillChildren()
 * - generateRandomNodeName()
 * - echoHelpAndDie()
 */

/**
 * Minimum node name lenght
 */
define('MINNODENAMELENGHT', 3);
/**
 * Maximun node name lenght
 */
define('MAXNODENAMELENGHT', 15);

/**
 * Fill DOMNode with random children
 *
 * @param DOMNode $node Current DOMNode
 * @param int $currentLevel Current node depth
 * @global DOMDocument Current DOMDocument
 * @global int Maximum depth
 * @global int Minimum children
 * @global int Maximum children
 */
function fillChildren($node, $currentLevel)
{
    global $dom, $maxlevel, $minchildren, $maxchildren;

    if ($currentLevel <= $maxlevel)
    {
        $nextLevel = $currentLevel + 1;

        for ($child = 0, $children = rand($minchildren, $maxchildren); $child < $children; $child++)
        {
            $newNode = $dom->createElement('treeNode');
            $newNodeAttribute = $dom->createAttribute('name');
            $newNodeAttribute->value = generateRandomNodeName();
            $newNode->appendChild($newNodeAttribute);

            $nextNode = $node->appendChild($newNode);

            fillChildren($nextNode, $nextLevel);
        }
    }

    return;
}

/**
 * Generate random node name as a string of [a-zA-Z0-9] of random lenght between 3 and 15
 *
 * @global int Minimum node name lenght
 * @global int Maximum node name lenght
 * @return string
 */
function generateRandomNodeName()
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = '', $length = rand(MINNODENAMELENGHT, MAXNODENAMELENGHT); $i < $length; $i++)
    {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

/**
 * Echo help message and terminate
 */
function echoHelpAndDie()
{
    echo 'Usage: 1.php [-h] -l <maxlevel> -i <minchildren> -a <maxchildren> [-o <outputfile>]'.PHP_EOL.
        PHP_EOL.
        '  -h, --help         This help'.PHP_EOL.
        '  -l, --maxlevel     Maximum depth (>0)'.PHP_EOL.
        '  -i, --minchildren  Minimum number of node children (>0)'.PHP_EOL.
        '  -a, --maxchildren  Maximum number of node children (>=minchildren)'.PHP_EOL.
        '  -o, --output       File to save result XML'.PHP_EOL.
        PHP_EOL;
    die();
}

if (PHP_SAPI === 'cli' or empty($_SERVER['REMOTE_ADDR']))
{
    // init from args
    $shortopts = 'hl:i:a:o:';

    $longopts = array(
        'help',
        'output:',
        'maxlevel:',
        'minchildren:',
        'maxchildren:',
    );

    $options = getopt($shortopts, $longopts);

    if (array_key_exists('help', $options) or array_key_exists('h', $options))
    {
        echoHelpAndDie();
    }
    else if (!(
        (array_key_exists('l', $options) or array_key_exists('maxlevel', $options))
        and (array_key_exists('i', $options) or array_key_exists('minchildren', $options))
        and (array_key_exists('a', $options) or array_key_exists('maxchildren', $options)))
    )
    {
        echoHelpAndDie();
    }

    $maxlevel = (array_key_exists('l', $options) ? $options['l'] : $options['maxlevel']);
    $minchildren = (array_key_exists('i', $options) ? $options['i'] : $options['minchildren']);
    $maxchildren = (array_key_exists('a', $options) ? $options['a'] : $options['maxchildren']);

    if (!(0 < $maxlevel and 0 < $minchildren and $minchildren <= $maxchildren))
    {
        echoHelpAndDie();
    }

    $dom = new DOMDocument('1.0', 'utf-8');

    $rootNode = $dom->createElement('treeNode');
    $rootNodeAttribute = $dom->createAttribute('name');
    $rootNodeAttribute->value = 'root';
    $rootNode->appendChild($rootNodeAttribute);

    $root = $dom->appendChild($rootNode);

    $nextLevel = 1;

    fillChildren($root, $nextLevel);

    $xml = $dom->saveXML();

    if (array_key_exists('output', $options) or array_key_exists('o', $options))
    {
        $outputFile = (array_key_exists('o', $options) ? $options['o'] : $options['output']);
        if (false === file_put_contents($outputFile, $xml))
        {
            throw new Exception('File write error');
        }
        echo 'XML successfully writed to file'.PHP_EOL;
    }
    else
    {
        echo $xml;
    }
}
else if (isset($_REQUEST))
{
    throw new Exception('CLI only');
}