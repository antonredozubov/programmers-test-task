<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Fibonachi
 *
 * @category  ProgrammerTask
 * @package   M
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

function find_fibonachi($n)
{
    // first element in row
    $fibonachi_n_minus_2 = 0;
    // second element in row
    $fibonachi_n_minus_1 = 1;

    if (0 == $n)
    {
        $fibonachi_n = $fibonachi_n_minus_2;
    }
    else if (1 == $n)
    {
        $fibonachi_n = $fibonachi_n_minus_1;
    }
    else
    {
        for ($i = 3; $i <= $n; $i++)
        {
            // get next element
            $fibonachi_n = $fibonachi_n_minus_1 + $fibonachi_n_minus_2;

            // shift previous elements
            $fibonachi_n_minus_2 = $fibonachi_n_minus_1;
            $fibonachi_n_minus_1 = $fibonachi_n;
        }
    }

    return $fibonachi_n;
}

$n = 12;
echo 'n(',$n,')=',find_fibonachi($n),"\n";