<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Find a path from root row to bottom row in graph.
 *
 * We get this tree as pyramid, every number of a row is connected to two other numbers in the row below it. 
 *
 * PHP 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ProgrammerTask
 * @package   L
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2013 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

/**
* Class and Function List:
* Function list:
* - BFS_Max_Sum_path()
* - BFS_Get_path()
* - __construct()
* - _tryToAddParent()
* - _tryToAddLeftParent()
* - _tryToAddRightParent()
* - traverse()
* - _tryToAddChildToStack()
* - _tryToAddLeftChildToStack()
* - _tryToAddRightChildToStack()
* - get()
* Classes list:
* - BFS
*/

// according to the task:
// - we have balanced binary tree

/**
 * Find unique path with max sum
 * 
 * @param array $path array of one path
 * 
 * @return bool
 */
function BFS_Max_Sum_path($path)
{
    global $max_sum;

    return($max_sum == array_sum($path));
}

/**
 * Get path as a string
 * 
 * @param array $arr array of one path
 * 
 * @return string
 */
function BFS_Get_path($arr)
{
    return join($arr, '-');
}

/**
 * Main class
 * 
 * @category ProgrammerTask
 * @package  L
 * @author   Anton Redozubov <anton@redozubov.ru>
 * @license  http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link     http://redozubov.ru
 */
class BFS
{
    private 
        $_initial_graph_array = array(),
        $_total_graph_nodes   = -1,
        $_total_graph_lines   = -1,
        $_total_graph_rows    = 0,
        $_max_graph_row_nodes = 0,
        $_graph_array         = array();

    private
        $_all_paths           = array(),
        $_tree_stack          = array();

    public
        $max_sum              = 0,
        $total_iterations     = -1;
    
    /**
     * Prepare graph array for work
     * 
     * @param string $initial_graph_string initial pyramid 
     */
    public function __construct($initial_graph_string)
    {
        $initial_graph_rows = explode(';', $initial_graph_string);

        foreach ($initial_graph_rows as $row => $initial_graph_row)
        {
            $initial_graph_row_cells = explode(',', $initial_graph_row);

            foreach ($initial_graph_row_cells as $cell => $initial_graph_row_cell)
            {
                if ('' <> $initial_graph_row_cell)
                {
                    if ($row > $this->_total_graph_rows)
                    {
                        $this->_total_graph_rows = $row;
                    }

                    if ($cell > $this->_max_graph_row_nodes)
                    {
                        $this->_max_graph_row_nodes = $cell;
                    }

                    $this->_total_graph_nodes++;

                    $this->_initial_graph_array[$row][$cell]['value'] = $initial_graph_row_cell;
                    $this->_initial_graph_array[$row][$cell]['id'] = $this->_total_graph_nodes;

                    $this->_graph_array[$this->_total_graph_nodes] = array(
                        'number' => $initial_graph_row_cell, 
                        'children' => array(
                            'left' => array(
                                'id' => null,
                                'visited' => false, 
                                ),
                            'right' => array(
                                'id' => null,
                                'visited' => false, 
                                ),
                            ),
                        );
                
                    $parent_row = $row-1;
                    $left_parent_cell = $cell-1;
                    $right_parent_cell = $cell;

                    $this->_tryToAddLeftParent($parent_row, $left_parent_cell, $this->_total_graph_nodes);
                    $this->_tryToAddRightParent($parent_row, $right_parent_cell, $this->_total_graph_nodes);
                }
            }
        }
    }

    /**
     * Try to add child id for parent node
     * Check if parent exists. Really - no sence. 
     * 
     * @param string $parent_side       left, right
     * @param int    $parent_row        parent row id
     * @param int    $parent_cell       parent cell id
     * @param int    $total_graph_nodes current node id
     * 
     * @return void
     */

    private function _tryToAddParent($parent_side, $parent_row, $parent_cell, $total_graph_nodes)
    {
        if (
            array_key_exists($parent_row, $this->_initial_graph_array)
            and array_key_exists($parent_cell, $this->_initial_graph_array[$parent_row])
            )
        {
            $this->_total_graph_lines++;
            $this->_graph_array[$this->_initial_graph_array[$parent_row][$parent_cell]['id']]['children'][$parent_side]['id'] = $total_graph_nodes;
        }
    }

    /**
     * Wrapper for left parent
     * 
     * @param int $parent_row        parent row id
     * @param int $parent_cell       parent cell id
     * @param int $total_graph_nodes current node id
     * 
     * @return void
     */
    private function _tryToAddLeftParent($parent_row, $parent_cell, $total_graph_nodes)
    {
        // left parent = right child
        $this->_tryToAddParent('right', $parent_row, $parent_cell, $total_graph_nodes);
    }

    /**
     * Wrapper for rigth parent
     * 
     * @param int $parent_row        parent row id
     * @param int $parent_cell       parent cell id
     * @param int $total_graph_nodes current node id
     * 
     * @return void
     */
    private function _tryToAddRightParent($parent_row, $parent_cell, $total_graph_nodes)
    {
        // right parent = left child
        $this->_tryToAddParent('left', $parent_row, $parent_cell, $total_graph_nodes);
    }

    /**
     * Traverse graph
     * 
     * @return array
     */
    public function traverse()
    {
        $current_node = 0;
        array_push($this->_tree_stack, array('id' => $current_node, 'path' => (array)$this->_graph_array[$current_node]['number']));

        while ($node = array_pop($this->_tree_stack))
        {
            $current_node = $node['id'];
            $path_to_parent = $node['path'];

            $this->total_iterations++;
               
            if (
                (
                    !is_null($this->_graph_array[$current_node]['children']['left']['id'])
                    and !$this->_graph_array[$current_node]['children']['left']['visited']
                    )
                or 
                (
                    !is_null($this->_graph_array[$current_node]['children']['right']['id'])
                    and !$this->_graph_array[$current_node]['children']['right']['visited']
                    )
            ) 
            {
                $this->_tryToAddLeftChildToStack($current_node, $path_to_parent);
                $this->_tryToAddRightChildToStack($current_node, $path_to_parent);
            }
            else
            {
                $current_path_sum = array_sum($path_to_parent);
                array_push($this->_all_paths, $path_to_parent);
                if ($this->max_sum < $current_path_sum)
                {
                    $this->max_sum = $current_path_sum;
                }
            }
        }

        return $this->_all_paths;
    }

    /**
     * Try to add child to stack.
     * If child exists and we have not visited child.
     * 
     * @param string $child_side     left, right
     * @param int    $current_node   current node
     * @param array  $path_to_parent path from root to current node
     * 
     * @return void
     */
    private function _tryToAddChildToStack($child_side, $current_node, $path_to_parent)
    {
        if (
            !is_null($this->_graph_array[$current_node]['children'][$child_side]['id'])
            and !$this->_graph_array[$current_node]['children'][$child_side]['visited']
            )
        {
            $this->_graph_array[$current_node]['children'][$child_side]['visited'] = true;
            $path_to_new_parent = array_merge($path_to_parent, (array)$this->_graph_array[$this->_graph_array[$current_node]['children'][$child_side]['id']]['number']);
            array_push($this->_tree_stack, array('id' => $this->_graph_array[$current_node]['children'][$child_side]['id'], 'path' => $path_to_new_parent));
        }
    }

    /**
     * Wrapper for left child
     * 
     * @param int   $current_node   current node
     * @param array $path_to_parent path from root to current node
     * 
     * @return void
     */
    private function _tryToAddLeftChildToStack($current_node, $path_to_parent)
    {
        $this->_tryToAddChildToStack('left', $current_node, $path_to_parent);
    }

    /**
     * Wrapper for right child
     * 
     * @param int   $current_node   current node
     * @param array $path_to_parent path from root to current node
     * 
     * @return void
     */
    private function _tryToAddRightChildToStack($current_node, $path_to_parent)
    {
        $this->_tryToAddChildToStack('right', $current_node, $path_to_parent);
    }

    /**
     * Dirty hack - get private variable
     * 
     * @param string $value variable name
     * 
     * @return mixed
     */
    public function get($value)
    {
        if (property_exists($this, $value))
        {
            return $this->$value;
        }
    }
}

$bfs6 = new BFS('1;19,4;4,3,19;1234,123,342,1234;12,12,12,12,12;18,14,15,16,17,18;');

var_dump(
    '6 row',
    'nodes',         $bfs6->get('_total_graph_nodes')+1,
    'lines',         $bfs6->get('_total_graph_lines')+1,
    'max rows',      $bfs6->get('_total_graph_rows')+1,
    'max row nodes', $bfs6->get('_max_graph_row_nodes')+1
    );

$all_paths6 = $bfs6->traverse();

$max_sum = $bfs6->max_sum;

$max_sum_paths6 = array_filter($all_paths6, 'BFS_Max_Sum_path');

$result6 = array_unique(array_map('BFS_Get_path', $max_sum_paths6));

var_dump(
    'result',           $result6,
    'total_iterations', $bfs6->total_iterations
    );

$bfs5 = new BFS('1;19,4;4,3,19;1234,123,342,1234;12,12,12,12,12;');

var_dump(
    '5 row',
    'nodes',         $bfs5->get('_total_graph_nodes')+1,
    'lines',         $bfs5->get('_total_graph_lines')+1,
    'max rows',      $bfs5->get('_total_graph_rows')+1,
    'max row nodes', $bfs5->get('_max_graph_row_nodes')+1
    );

$all_paths5 = $bfs5->traverse();

$max_sum = $bfs5->max_sum;

$max_sum_paths5 = array_filter($all_paths5, 'BFS_Max_Sum_path');

$result5 = array_unique(array_map('BFS_Get_path', $max_sum_paths5));


var_dump(
    'result',           $result5,
    'total_iterations', $bfs5->total_iterations
    );


