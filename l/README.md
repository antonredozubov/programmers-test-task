# Three tasks

## Find a path from root row to bottom row in graph.

We get this tree as a pyramid, every number of a row is connected to two other numbers in the row below it.

## Find missing number in table

We get the table with 99 numbers from 1 to 100.

## Dutch national flag problem

Sort very big random array of (0,1,2) in one pass