<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Dutch national flag problem
 *
 * Sort very big random array of (0,1,2) in one pass
 *
 * PHP 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   ProgrammerTask
 * @package    L
 * @author     Anton Redozubov <anton@redozubov.ru>
 * @copyright  2013 Anton Redozubov
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

/**
* Class and Function List:
* Function list:
* - swap()
* Classes list:
*/

$arr = array(1, 2, 1, 2, 1, 2, 0, 0, 0, 1, 2, 0, 0, 2, 1, 2, 0, 0, 0, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1, 2, 1, 2, 0, 0, 0, 1);

function swap(&$arr, $p, $k)
{
    $swap = $arr[$p];
    $arr[$p] = $arr[$k];
    $arr[$k] = $swap;
}

$p_index = 0;
$k_index = sizeof($arr) - 1;

$p = 0;
$k = 2;

var_dump('initial array',join($arr,''),'array size', sizeof($arr));

for ($i = 0, $j = 0; $i <= $k_index; $j++)
{
    if ($arr[$i] == $p)
    {
        swap($arr, $i, $p_index);
        $p_index++;
        $i++;
    }
    else if ($arr[$i] >= $k)
    {
        swap($arr, $i, $k_index);
        $k_index--;
    }
    else 
    {
        $i++;
    }
}
var_dump('result array',join($arr,''),'total iterations',$j);
    